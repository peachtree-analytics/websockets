<?php

declare(strict_types=1);

namespace Peachtree\Websocket;

final class MessageFactory
{
    private Message $message;

    private function __construct(?string $reference = null)
    {
        $this->message = (new Message())->setRef($reference);
    }

    public function acknowledge(string $message): Message
    {
        return $this->message
            ->setAction('ack')
            ->setPayload(['message' => $message]);
    }

    /**
     * @param string $message
     * @param array<string, mixed> $extra
     * @return Message
     */
    public function error(string $message, array $extra = []): Message
    {
        return $this->message
            ->setAction('error')
            ->setPayload(array_merge($extra, ['message' => $message]));
    }

    public static function make(string $reference = null): self
    {
        return new self($reference);
    }

    public function validationException(string $message, string ...$reasons): Message
    {
        return $this->error($message, [
            'code' => 422,
            'reason' => $reasons
        ]);
    }
}

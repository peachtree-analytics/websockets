<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Handler;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\Response;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\MessageFactory;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Rules\Each;
use Respect\Validation\Rules\Key;
use Respect\Validation\Rules\StringType;
use Respect\Validation\Validator;

final class ChannelManager extends MessageHandler
{
    /** @var callable */
    private $authorizationCallback;

    /** @var Validator */
    private Validator $validator;

    /**
     * ChannelManager constructor.
     * @param callable|null $authorizationCallback
     */
    public function __construct(callable $authorizationCallback = null)
    {
        $this->authorizationCallback = $authorizationCallback ?: fn () => true;
        $this->validator = (new Validator())
            ->setName('channel management validation')
            ->addRules([
                new Key(
                    'subscribe',
                    new Each(new StringType())
                ),
                new Key(
                    'unsubscribe',
                    new Each(new StringType())
                )
            ]);
    }

    /**
     * @inheritDoc
     */
    protected function handle(Message $message, State &$state): Generator
    {
        try {
            $this->validator->assert($validated = $message->getPayload());
        } catch (NestedValidationException $e) {
            yield new Response(MessageFactory::make($message->getRef())->validationException(
                $e->getMainMessage(),
                ...$e->getMessages()
            ));
            return;
        }

        $subscribe = 0;
        foreach ($validated['subscribe'] ?? [] as $channel) {
            if (!($this->authorizationCallback)($channel, $state)) {
                yield new Response(
                    MessageFactory::make($message->getRef())
                        ->error('You are not authorized to join "' . $channel . '"', ['code' => 403])
                );
                continue;
            }
            $state->subscribeToChannel($channel);
            $subscribe++;
        }

        $unsubscribe = 0;
        foreach ($validated['unsubscribe'] ?? [] as $channel) {
            $state->unsubscribeFromChannel($channel);
            $unsubscribe++;
        }

        yield new Response(
            MessageFactory::make($message->getRef())->acknowledge(
                sprintf(
                    'Subscribed to %d and unsubscribed from %d channels.',
                    $subscribe,
                    $unsubscribe,
                )
            )
        );
    }

    /**
     * @inheritDoc
     */
    public function shouldHandle(string $messageAction): bool
    {
        return $messageAction === 'channel';
    }
}

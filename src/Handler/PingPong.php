<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Handler;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\Response;
use Peachtree\Websocket\Message;

final class PingPong extends MessageHandler
{
    /**
     * @inheritDoc
     */
    protected function handle(Message $message, State &$state): Generator
    {
        yield new Response((new Message())->setAction('pong')->setRef($message->getRef()));
    }

    /**
     * @inheritDoc
     */
    public function shouldHandle(string $messageAction): bool
    {
        return $messageAction === 'ping';
    }
}

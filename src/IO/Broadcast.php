<?php

declare(strict_types=1);

namespace Peachtree\Websocket\IO;

use Peachtree\Websocket\IO\Interfaces\Broadcast as BroadcastInterface;
use Peachtree\Websocket\Message;

final class Broadcast implements BroadcastInterface
{
    /** @var array|string[] */
    private array $channels;

    private Message $message;

    public function __construct(Message $message, string ...$channels)
    {
        $this->channels = $channels;
        $this->message = $message;
    }

    /**
     * Returns a list of channels being broadcast to.
     *
     * @return string[]
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    /**
     * Return the message associated with this class.
     *
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }
}

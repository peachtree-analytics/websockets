<?php

declare(strict_types=1);

namespace Peachtree\Websocket\IO\Interfaces;

use Peachtree\Websocket\Message;

interface Response
{
    /**
     * Return the message associated with this class.
     *
     * @return Message
     */
    public function getMessage(): Message;
}

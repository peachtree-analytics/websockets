<?php

declare(strict_types=1);

namespace Peachtree\Websocket\IO\Interfaces;

interface Broadcast extends Response
{
    /**
     * Returns a list of channels being broadcast to.
     *
     * @return string[]
     */
    public function getChannels(): array;
}

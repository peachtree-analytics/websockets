<?php

declare(strict_types=1);

namespace Peachtree\Websocket\IO;

use Peachtree\Websocket\IO\Interfaces\Broadcast as BroadcastInterface;
use Peachtree\Websocket\Message;

final class DeferredBroadcast implements BroadcastInterface
{
    /** @var array|string[] */
    private array $channels;

    private ?Message $message = null;

    private $resolver;

    public function __construct(callable $resolver, string ...$channels)
    {
        $this->channels = $channels;
        $this->resolver = $resolver;
    }

    /**
     * Returns a list of channels being broadcast to.
     *
     * @return string[]
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    /**
     * Return the message associated with this class.
     *
     * @return Message
     */
    public function getMessage(): Message
    {
        if (is_null($this->message)) {
            $this->message = ($this->resolver)();
        }
        return $this->message;
    }
}

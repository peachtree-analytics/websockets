<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Connection;

use Exception;
use JsonException;
use Peachtree\Websocket\IO\Interfaces\Broadcast;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\MessageFactory;
use Peachtree\Websocket\Routing\RouterInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Rules as Rule;
use Respect\Validation\Validator;
use SplObjectStorage;

final class Handler implements MessageComponentInterface
{
    private SplObjectStorage $clients;
    private RouterInterface $router;

    private Validator $singleMessageValidator;
    private Validator $multiMessageValidator;

    public function __construct(RouterInterface $router)
    {
        $this->clients = new SplObjectStorage();
        $this->router = $router;

        $this->singleMessageValidator = new Validator(
            new Rule\KeyNested('action', new Rule\StringType()),
            new Rule\KeyNested('payload', new Rule\ObjectType()),
            new Rule\KeyNested('ref', new Rule\OneOf(new Rule\StringType(), new Rule\NullType()), false),
        );
        $this->multiMessageValidator = new Validator(
            new Rule\Each($this->singleMessageValidator)
        );
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws Exception
     */
    public function onOpen(ConnectionInterface $conn): void
    {
        $state = new State(
            uniqid('peachtree_websocket_connection_'),
            (object)['remoteAddress' => property_exists($conn, 'remoteAddress') ? $conn->remoteAddress : null]
        );
        $state->see();
        $this->clients->attach($conn, $state);
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws Exception
     */
    public function onClose(ConnectionInterface $conn): void
    {
        $this->clients->detach($conn);
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param ConnectionInterface $conn
     * @param Exception $e
     * @throws Exception
     */
    public function onError(ConnectionInterface $conn, Exception $e): void
    {
        $this->router->handleError($e, $this->clients[$conn]);
        $conn->close();
    }

    /**
     * Triggered when a client sends data through the socket
     * @param ConnectionInterface $from The socket/connection that sent the message to your application
     * @param string $msg The message received
     * @throws Exception
     */
    public function onMessage(ConnectionInterface $from, $msg): void
    {
        /** @var State $state */
        $state =& $this->clients[$from];

        // Note that we have seen this client
        $state->see();

        try {
            // Decode the messages and run basic validation on them
            $messages = json_decode($msg, false, 32, JSON_THROW_ON_ERROR);

            // If this is an array, pass validation as if this is a list of objects
            if (is_array($messages)) {
                $this->multiMessageValidator->assert($messages);
            } else {
                $this->singleMessageValidator->assert($messages);
                $messages = [$messages];
            }
        } catch (NestedValidationException $e) {
            // This message was invalid for some reason. Let the client know.
            $from->send(
                (string)MessageFactory::make($validated['ref'] ?? null)->validationException(
                    $e->getMainMessage(),
                    ...$e->getMessages()
                )
            );
            return;
        } catch (JsonException $e) {
            $from->send((string)MessageFactory::make()->error($e->getMessage()));
            return;
        }

        array_walk($messages, static function (&$message): void {
            $message = (new Message())
                ->setAction($message->action)
                ->setPayload(static::parseMessagePayload($message->payload))
                ->setRef($message->ref ?? null);
        });

        foreach ($messages as $message) {
            foreach ($this->router->handleMessage($message, $state) as $response) {

                // If this response is a broadcast, then send to channel subscribers
                if ($response instanceof Broadcast) {
                    $broadcastChannels = $response->getChannels();

                    /** @var ConnectionInterface $client */
                    foreach ($this->clients as $client) {
                        /** @var State $state */
                        $state = $this->clients[$client];

                        foreach ($broadcastChannels as $channel) {
                            if ($state->subscribedToChannel($channel)) {
                                $client->send((string)$response->getMessage());
                                break;
                            }
                        }
                    }
                    continue;
                }

                // Otherwise, just send to the sender
                $from->send((string)$response->getMessage());
            }
        }
    }

    /**
     * Recursively casts a variable to an array where possible.
     *
     * @param mixed $el
     * @return mixed
     */
    private static function parseMessagePayload($el)
    {
        if (is_object($el)) {
            $el = (array)$el;
        }
        if (is_array($el)) {
            return array_map(__METHOD__, $el);
        }
        return $el;
    }
}

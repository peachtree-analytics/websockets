<?php

declare(strict_types=1);

namespace Peachtree\Websocket;

use Peachtree\Websocket\Connection\Handler;
use Peachtree\Websocket\Routing\Router;
use Peachtree\Websocket\Routing\RouterInterface;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory as LoopFactory;
use React\EventLoop\LoopInterface;
use React\Socket\Server as Reactor;

final class Server
{
    private Handler $handler;

    private LoopInterface $loop;

    private Reactor $reactor;

    public function __construct(string $address='0.0.0.0', int $port=8080, RouterInterface $router=null, LoopInterface $loop=null)
    {
        $this->loop = $loop ?: LoopFactory::create();
        $this->handler = new Handler($router ?: new Router());
        $this->reactor = new Reactor(sprintf('%s:%d', $address, $port), $this->loop);
    }

    /**
     * Creates a HTTP WS Server. Useful for JavaScript WebSocket objects.
     *
     * @return IoServer
     */
    public function http(): IoServer
    {
        return new IoServer(
            new HttpServer(
                new WsServer(
                    $this->handler
                )
            ),
            $this->reactor,
            $this->loop
        );
    }

    /**
     * Creates a raw socket server.
     * Handy for telnet connections without any other overhead.
     *
     * @return IoServer
     */
    public function socket(): IoServer
    {
        return new IoServer(
            $this->handler,
            $this->reactor,
            $this->loop
        );
    }
}

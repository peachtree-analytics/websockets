<?php

declare(strict_types=1);

namespace Peachtree\Websocket;

use JsonSerializable;
use function json_encode;

/**
 * Class Message
 * @package Peachtree\Websocket
 */
final class Message implements JsonSerializable
{

    /** @var string The name of the action this message is for */
    private string $action;

    /** @var array<string, mixed> The meaningful data for this message */
    private array $payload = [];

    /** @var string|null The reference for this message. Will be sent back in any response. */
    private ?string $ref = null;

    /**
     * Return the string representation of this message in JSON format.
     *
     * @return string
     */
    public function __toString(): string
    {
        return json_encode($this) ?: '{}';
    }

    /**
     * Returns an instance of the message factory.
     *
     * @param string|null $reference
     * @return MessageFactory
     */
    public static function factory(string $reference = null): MessageFactory
    {
        return MessageFactory::make($reference);
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return array<string, mixed>
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * Prepare this object for JSON serialization.
     * @return array{ref: string|null, action: string, payload: \stdClass}
     */
    public function jsonSerialize(): array
    {
        return [
            'ref' => $this->ref,
            'action' => $this->action,
            'payload' => (object)$this->payload
        ];
    }

    /**
     * @param string $action
     * @return Message
     */
    public function setAction(string $action): self
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @param array<string, mixed> $payload
     * @return Message
     */
    public function setPayload(array $payload): self
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @param string|null $ref
     * @return Message
     */
    public function setRef(?string $ref): self
    {
        $this->ref = $ref;
        return $this;
    }
}

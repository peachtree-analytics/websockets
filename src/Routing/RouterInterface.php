<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Routing;

use Exception;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\Collection;
use Peachtree\Websocket\Message;

interface RouterInterface
{
    /**
     * If an error is encountered when parsing message handlers, use this method to do appropriate logging/reporting.
     *
     * @param Exception $e
     * @param State $state
     */
    public function handleError(Exception $e, State $state): void;

    /**
     * Route the incoming message to the appropriate message handlers and return a response collection.
     *
     * @param Message $message
     * @param State $state
     * @return Collection
     */
    public function handleMessage(Message $message, State &$state): Collection;
}

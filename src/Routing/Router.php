<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Routing;

use Exception;
use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\Handler\MessageHandler;
use Peachtree\Websocket\IO\Collection;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\Middleware\Middleware;

final class Router implements RouterInterface
{
    /** @var callable[] $errorHandlers */
    private static array $errorHandlers = [];

    /** @var MessageHandler[] */
    private static array $messageHandlers = [];

    /** @var Middleware[] */
    private static array $middleware = [];

    public static function addErrorHandler(callable ...$handlers): void
    {
        array_push(self::$errorHandlers, ...$handlers);
    }

    public static function addMessageHandler(MessageHandler ...$handlers): void
    {
        array_push(self::$messageHandlers, ...$handlers);
    }

    public static function addMiddleware(Middleware ...$middleware): void
    {
        array_push(self::$middleware, ...$middleware);
    }

    /**
     * If an error is encountered when parsing message handlers, use this method to do appropriate logging/reporting.
     *
     * @param Exception $e
     * @param State $state
     */
    public function handleError(Exception $e, State $state): void
    {
        foreach (self::$errorHandlers as $errorHandler) {
            $errorHandler($e, $state);
        }
    }

    /**
     * Route the incoming message to the appropriate message handlers and return a response collection.
     *
     * @param Message $message
     * @param State $state
     * @return Collection
     */
    public function handleMessage(Message $message, State &$state): Collection
    {
        return new Collection(
            self::pipeline(static function (Message $message, State &$state): Generator {
                $messageAction = $message->getAction();
                foreach (self::$messageHandlers as $messageHandler) {
                    if (!$messageHandler->shouldHandle($messageAction)) {
                        continue;
                    }
                    yield from $messageHandler($message, $state);
                }
            })($message, $state)
        );
    }

    /**
     * Reduces the application middleware down to a recursive function that calls each in turn, with the result of the
     * $initial method as the input.
     *
     * The reduced pipeline is cached until new middleware is added.
     *
     * @param callable $initial
     * @return callable
     */
    private static function pipeline(callable $initial): callable
    {
        static $middlewareCount = 0;
        static $pipeline = null;

        if (is_null($pipeline) || $middlewareCount !== count(self::$middleware)) {
            $middlewareCount = count(self::$middleware);
            $pipeline = array_reduce(
                self::$middleware,
                static function (callable $carry, Middleware $middleware): callable {
                    return static function (Message $message, State &$state) use ($carry, $middleware): Generator {
                        yield from $middleware($carry($message, $state), $message, $state);
                    };
                },
                $initial
            );
        }

        return $pipeline;
    }
}

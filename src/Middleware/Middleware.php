<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Middleware;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\IO\Interfaces\Response;
use Peachtree\Websocket\Message;

interface Middleware
{
    /**
     * Is called when a message is due to be handled.
     *
     * To pass through the responses, you must `yield from $responses;`.
     *
     * @param Generator<Response> $responses
     * @param Message $input
     * @param State $state
     * @return Generator<Response>
     */
    public function __invoke(Generator $responses, Message $input, State &$state): Generator;
}

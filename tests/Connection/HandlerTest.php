<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Connection;

use Exception;
use Generator;
use Peachtree\Websocket\Connection\Handler;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\Handler\MessageHandler;
use Peachtree\Websocket\IO\Response;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\MessageFactory;
use Peachtree\Websocket\Routing\Router;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Ratchet\ConnectionInterface;
use Ratchet\Server\IoServer;

final class HandlerTest extends TestCase
{
    public function testConnectionHandler(): void
    {
        $router = new Router();
        $router::addMessageHandler(
            static::messageHandler("one"),
            static::messageHandler("two"),
        );

        $handler = new Handler($router);

        $connection = new DebugConnection();

        $handler->onOpen($connection);

        $handler->onMessage($connection, '{"action":"one","payload":{},"ref":"asdf"}');
        $this->assertEquals(
            ['{"ref":"asdf","action":"ack","payload":{"message":"handled::one"}}'],
            $connection->messages
        );

        $connection->messages = [];

        $handler->onMessage(
            $connection,
            '[{"action":"one","payload":{},"ref":"abc"},{"action":"two","payload":{},"ref":"def"}]'
        );
        $this->assertEquals(
            [
                '{"ref":"abc","action":"ack","payload":{"message":"handled::one"}}',
                '{"ref":"def","action":"ack","payload":{"message":"handled::two"}}',
            ],
            $connection->messages
        );
    }

    public function testErrorLogging(): void
    {
        $logger = new class {
            public array $logged = [];
            public function __invoke(Exception $e, State $state): void
            {
                $this->logged[] = [$e, $state];
            }
        };

        $router = new Router();
        $router::addErrorHandler($logger);
        $router::addMessageHandler(
            new class extends MessageHandler {
                protected function handle(Message $message, State &$state): Generator
                {
                    throw new Exception('Foo Bar');
                }
                public function shouldHandle(string $messageAction): bool
                {
                    return $messageAction === 'test-error';
                }
            }
        );

        $server = IoServer::factory(new Handler($router), 8080);

        $conn = $this->createMock(\React\Socket\ConnectionInterface::class);
        $conn->stream = fopen('data://text/plain,foo bar', 'r');

        $server->handleConnect($conn);
        $server->handleData('{"ref":null,"payload":{},"action":"test-error"}', $conn);

        $this->assertCount(1, $logger->logged);
        $this->assertInstanceOf(Exception::class, $logger->logged[0][0]);
        $this->assertInstanceOf(State::class, $logger->logged[0][1]);
    }

    public function testMessageHandlerPayload(): void
    {
        $expectedData = [
            'foo' => 'bar',
            'bing' => [
                42,
                'asdf',
            ],
        ];
        $router = new Router();
        $router::addMessageHandler(
            new class($this, $expectedData) extends MessageHandler {
                private Assert $test;
                private array $equals;
                public function __construct(Assert $test, array $equals)
                {
                    $this->test = $test;
                    $this->equals = $equals;
                }
                protected function handle(Message $message, State &$state): Generator
                {
                    yield new Response($message);
                    $this->test->assertEquals($this->equals, $message->getPayload());
                }
                public function shouldHandle(string $messageAction): bool
                {
                    return $messageAction === 'test';
                }
            }
        );

        $handler = new Handler($router);

        $connection = new DebugConnection();

        $message = json_encode(['ref' => 'aaa', 'action' => 'test', 'payload' => $expectedData]);

        $handler->onOpen($connection);
        $handler->onMessage($connection, $message);

        $this->assertEquals(
            [$message],
            $connection->messages
        );
    }

    private static function messageHandler(string $key): MessageHandler
    {
        return new class($key) extends MessageHandler {
            private string $key;

            public function __construct(string $key)
            {
                $this->key = $key;
            }

            protected function handle(Message $message, State &$state): Generator
            {
                yield new Response(MessageFactory::make($message->getRef())->acknowledge("handled::{$this->key}"));
            }

            public function shouldHandle(string $messageAction): bool
            {
                return $messageAction === $this->key;
            }
        };
    }
}

final class DebugConnection implements ConnectionInterface
{
    public array $messages = [];
    public function send($data): void
    {
        $this->messages[] = $data;
    }
    public function close(): void
    {
        // nothing to do
    }
}

<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Middleware;

use Generator;
use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\Handler\PingPong;
use Peachtree\Websocket\Message;
use Peachtree\Websocket\Middleware\Debugger;
use Peachtree\Websocket\Middleware\Middleware;
use Peachtree\Websocket\Routing\Router;
use PHPUnit\Framework\TestCase;

final class MiddlewareRegistrationTest extends TestCase
{
    public function testMiddlewareRegistration(): void
    {
        // For this test, lets just use the 'ping pong' handler.
        Router::addMessageHandler(new PingPong());

        // Register a basic middleware that just prints a 'before' and 'after'
        Router::addMiddleware(
            new class implements Middleware {
                public function __invoke(Generator $responses, Message $input, State &$state): Generator
                {
                    echo 'before foo', PHP_EOL;
                    yield from $responses;
                    echo 'after foo', PHP_EOL;
                }
            },
            new class implements Middleware {
                public function __invoke(Generator $responses, Message $input, State &$state): Generator
                {
                    echo 'before bar', PHP_EOL;
                    yield from $responses;
                    echo 'after bar', PHP_EOL;
                }
            },
            new Debugger(), // Just for shits&giggles
        );

        $router = new Router();
        $state = new State('Foo ID');

        ob_start();
        // cast the response to an array to run the internal generators
        iterator_to_array(
            $router->handleMessage(
                (new Message())
                    ->setAction('ping')
                    ->setPayload([])
                    ->setRef(null),
                $state
            )
        );
        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals(
            <<<TEXT
Foo ID
 < {"ref":null,"action":"ping","payload":{}}
before bar
before foo
 > {"ref":null,"action":"pong","payload":{}}
after foo
after bar
------------------------------------------------------------

TEXT,
            $output
        );
    }
}

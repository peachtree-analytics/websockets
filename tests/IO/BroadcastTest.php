<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\IO;

use Peachtree\Websocket\IO\Broadcast;
use Peachtree\Websocket\Message;
use PHPUnit\Framework\TestCase;

final class BroadcastTest extends TestCase
{
    public function testBroadcast(): void
    {
        $message = (new Message())
            ->setAction('foo')
            ->setPayload(['bar'])
            ->setRef('bing');

        $channels = ['foo', 'bar', 'baz'];

        $broadcast = new Broadcast($message, ...$channels);

        $this->assertEquals($message, $broadcast->getMessage());
        $this->assertEquals($channels, $broadcast->getChannels());
    }
}

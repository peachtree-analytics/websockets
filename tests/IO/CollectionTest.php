<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\IO;

use Peachtree\Websocket\IO\Collection;
use Peachtree\Websocket\IO\Interfaces\Response;
use PHPUnit\Framework\TestCase;

final class CollectionTest extends TestCase
{
    public function testCreatingCollections(): void
    {
        $responses = [
            $this->getMockBuilder(Response::class)->getMock(),
            $this->getMockBuilder(Response::class)->getMock(),
        ];
        foreach (Collection::make($responses) as $i => $response) {
            $this->assertEquals($responses[$i], $response);
        }
    }
}

<?php

declare(strict_types=1);

namespace Peachtree\Websocket\Tests\Handlers;

use Peachtree\Websocket\Connection\State;
use Peachtree\Websocket\Handler\PingPong;
use Peachtree\Websocket\Message;
use PHPUnit\Framework\TestCase;

final class PingPongTest extends TestCase
{
    public function testPingPongHandler(): void
    {
        $handler = new PingPong();

        $this->assertTrue($handler->shouldHandle('ping'));
        $this->assertFalse($handler->shouldHandle('pong'));

        $state = new State();
        $message = (new Message())->setAction('ping')->setRef('foo');
        $response = $handler($message, $state)
            ->current()
            ->getMessage();

        $this->assertEquals('pong', $response->getAction());
        $this->assertEquals([], $response->getPayload());
        $this->assertEquals('foo', $response->getRef());
    }
}
